/*
 * The MIT License
 *
 * Copyright 2017 ashwinrao.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.arconsultancy.utils.aws.ddb;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ashwin Rao
 */
public class TableItemOpsTest {
    
    public TableItemOpsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getItemOp method, of class TableItemOps.
     */
    @org.junit.Test
    public void testGetItemOp_PrimaryKey() {
        System.out.println("getItemOp");
        PrimaryKey primaryKey = null;
        TableItemOps instance = null;
        Item expResult = null;
        Item result = instance.getItemOp(primaryKey);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getItemOp method, of class TableItemOps.
     */
    @org.junit.Test
    public void testGetItemOp_GetItemSpec() {
        System.out.println("getItemOp");
        GetItemSpec spec = null;
        TableItemOps instance = null;
        Item expResult = null;
        Item result = instance.getItemOp(spec);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of putItemOp method, of class TableItemOps.
     */
    @org.junit.Test
    public void testPutItemOp_Map_Map() throws Exception {
        System.out.println("putItemOp");
        Map<KeySchemaElement, Object> keyMap = null;
        Map infoMap = null;
        TableItemOps instance = null;
        instance.putItemOp(keyMap, infoMap);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of putItemOp method, of class TableItemOps.
     */
    @org.junit.Test
    public void testPutItemOp_PrimaryKey_Map() {
        System.out.println("putItemOp");
        PrimaryKey primaryKey = null;
        Map infoMap = null;
        TableItemOps instance = null;
        instance.putItemOp(primaryKey, infoMap);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of deleteItem method, of class TableItemOps.
     */
    @org.junit.Test
    public void testDeleteItem() {
        System.out.println("deleteItem");
        PrimaryKey primaryKey = null;
        TableItemOps instance = null;
        instance.deleteItem(primaryKey);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of scanTable method, of class TableItemOps.
     */
    @org.junit.Test
    public void testScanTable() {
        System.out.println("scanTable");
        ScanSpec scanSpec = null;
        TableItemOps instance = null;
        ItemCollection<ScanOutcome> expResult = null;
        ItemCollection<ScanOutcome> result = instance.scanTable(scanSpec);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
