/*
 * The MIT License
 *
 * Copyright 2017 ashwinrao.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.arconsultancy.demos.aws.ddb;

import biz.arconsultancy.utils.aws.ddb.TableItemOps;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ashwinrao
 */
public class MoviesOps {

    // Access table
    private final TableItemOps moviesTable = new TableItemOps("Movies", "http://localhost:8000", "us-west-2");

    // Define item data
    private final Map<String, Object> infoMap = new HashMap<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        new MoviesOps().performActions();
    }

    public void performActions() {
        //putItemWithKeyMap();
        putItemWithPrimaryKey();
        //getItem(moviesTable);
        scanTable();
        //deleteItemWithPrimaryKey();
        //scanTable();
    }

    private void putItemWithPrimaryKey() {
        // the right way to add an item, by using the PrimaryKey class
        PrimaryKey primaryKey = new PrimaryKey("year", 2014, "title", "The crazies last stand");

        infoMap.put("plot", "The original boring plot");
        infoMap.put("rating", 1);
        
        moviesTable.putItemOp(primaryKey, infoMap);
    }

    private void putItemWithKeyMap() {
        // Not the preferred way to do this. Trying to add an item from
        // schema elements and not using the PrimaryKey class.
        // Helps understand the use of the table.description in TableItemOps
        // Define item Primary and Sort key
        Map<KeySchemaElement, Object> keyMap = new HashMap();
        keyMap.put(new KeySchemaElement("year", KeyType.HASH), 2015);
        keyMap.put(new KeySchemaElement("title", KeyType.RANGE), "Big new movie");

        infoMap.put("plot", "Nothing at all");
        infoMap.put("rating", 0);
        try {
            moviesTable.putItemOp(keyMap, infoMap);
        } catch (Exception e) {
            Logger.getLogger(MoviesOps.class.getName()).log(Level.SEVERE, "Unknown exception", e);
        }
    }

    private void getItem() {
        PrimaryKey primaryKey = new PrimaryKey("year", 2015, "title", "Big new movie");

        Item item = moviesTable.getItemOp(new GetItemSpec().withPrimaryKey(primaryKey));

        Logger.getLogger(MoviesOps.class.getName()).log(Level.INFO, "Item retrieval succeeded");
    }

    private void scanTable() {
        try {
            ScanSpec scanSpec = new ScanSpec()
                    .withProjectionExpression("#yr, title, info")
                    .withNameMap(new NameMap().with("#yr", "year"));
                    //.withFilterExpression("#yr between :start_yr and :end_yr").withNameMap(new NameMap().with("#yr", "year"))
                    //.withValueMap(new ValueMap().withNumber(":start_yr", 2014).withNumber(":end_yr", 2020));

            ItemCollection<ScanOutcome> moviesItems = moviesTable.scanTable(scanSpec);
            
            moviesItems.forEach((Item x) -> System.out.println(x.toString()));

            Logger.getLogger(MoviesOps.class.getName()).log(Level.INFO, "Table scan succeeded");
        } catch (AmazonDynamoDBException e) {
            Logger.getLogger(MoviesOps.class.getName()).log(Level.SEVERE, "Scan failed", e);
        } catch (Exception e) {
            Logger.getLogger(MoviesOps.class.getName()).log(Level.SEVERE, "Unknown exception", e);
        }
    }

    private void deleteItemWithPrimaryKey() {
        PrimaryKey primaryKey = new PrimaryKey("year", 2017, "title", "The return of the crazies");
        try {
            moviesTable.deleteItem(primaryKey);
        } catch (Exception e) {
            Logger.getLogger(MoviesOps.class.getName()).log(Level.SEVERE, "Unknown exception", e);
        }
    }
}
