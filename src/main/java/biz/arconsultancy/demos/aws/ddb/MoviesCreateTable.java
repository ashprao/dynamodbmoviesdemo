/*
 * The MIT License
 *
 * Copyright 2017 Ashwin.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.arconsultancy.demos.aws.ddb;

import biz.arconsultancy.utils.aws.ddb.DBConnection;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ashwin
 */
public class MoviesCreateTable {

    public static void main(String[] args) {
        DynamoDB dynamoDB = DBConnection.getDB("http://localhost:8000", "us-west-2");
        
        String tableName = "Movies";
        
        try {
            LOG.log(Level.INFO, "Attempting to create table. Please wait... ");
            
            List<KeySchemaElement> keySchema;
            keySchema = Arrays.asList(
                    new KeySchemaElement("year", KeyType.HASH),
                    new KeySchemaElement("title", KeyType.RANGE));
            
            List<AttributeDefinition> attributeDefinitions;
            attributeDefinitions = Arrays.asList(
                    new AttributeDefinition("year", ScalarAttributeType.N),
                    new AttributeDefinition("title", ScalarAttributeType.S));
            
            ProvisionedThroughput provisionedThroughput;
            provisionedThroughput = new ProvisionedThroughput(10L, 10L);
            
            Table table;
            table = dynamoDB.createTable(tableName, keySchema, attributeDefinitions, provisionedThroughput);
            
            // The table will not be ready for use until DynamoDB creates it and
            // sets its status to active. So we wait for it.
            table.waitForActive();
            
            LOG.log(Level.INFO, "Table creation successfull. Table status: {0}", table.getDescription());
            
            
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unable to create table; " + e.toString() + "; " + e.getMessage());
        }

    }
    private static final Logger LOG = Logger.getLogger(MoviesCreateTable.class.getName());

}
