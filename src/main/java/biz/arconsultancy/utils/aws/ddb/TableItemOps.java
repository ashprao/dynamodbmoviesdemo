/*
 * The MIT License
 *
 * Copyright 2017 ashwinrao.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package biz.arconsultancy.utils.aws.ddb;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ashwinrao
 */
public class TableItemOps {

    // Logger object
    private static final Logger LOG = Logger.getLogger(TableItemOps.class.getName());

    // DB to connect to
    private DynamoDB db;
    // Table to operate on
    private Table table;

    /**
     * Constructor
     *
     * @param tableName
     * @param location
     * @param region
     */
    public TableItemOps(
            String tableName,
            String location,
            String region) {
        db = DBConnection.getDB(location, region);
        table = db.getTable(tableName);
    }

    /**
     * Gets an item from the table using PrimaryKey
     *
     * @param primaryKey
     * @return
     */
    public Item getItemOp(PrimaryKey primaryKey) {
        return table.getItem(primaryKey);
    }

    /**
     * Gets an item from the table using GetItemSpec
     *
     * @param spec
     * @return
     */
    public Item getItemOp(GetItemSpec spec) {
        return table.getItem(spec);
    }

    /**
     * Adds an item to the table with a key schema element map Not the right way
     * to go about it. See the overloaded version of this function using the
     * PrimaryKey
     *
     * @param keyMap
     * @param infoMap
     * @throws java.lang.Exception
     */
    public void putItemOp(
            Map<KeySchemaElement, Object> keyMap,
            Map infoMap) throws Exception {

        LOG.log(Level.INFO, "Adding a new item...");

        // Get the table key schema
        TableDescription tableDescription = table.describe();
        List<KeySchemaElement> tableKeyList = tableDescription.getKeySchema();

        // Check if table key schema has same number of elements as the 
        // provided key map
        if (keyMap.size() != tableKeyList.size()) {
            throw new Exception("Key map does not match table key schema");
        }

        // Create the primary key object from key map as per the table key 
        // schema.
        PrimaryKey primaryKey = new PrimaryKey();
        for (Iterator<KeySchemaElement> iterator = tableKeyList.iterator(); iterator.hasNext();) {
            KeySchemaElement next = iterator.next();

            // Check if the key exists in the key map
            if (keyMap.containsKey(next)) {
                primaryKey.addComponent(next.getAttributeName(), keyMap.get(next));
            } else {
                // Key element is not found. Raise exception
                LOG.log(Level.SEVERE, "Required primary or sort key not found", keyMap);
                throw new Exception("Required primary or sort key not found");
            }
        }

        try {
            PutItemOutcome outcome = table
                    .putItem(new Item()
                            .withPrimaryKey(primaryKey)
                            .withMap("info", infoMap));
            LOG.log(Level.INFO, "Item successfully added.", outcome);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unable to add item.", e);
        }
    }

    /**
     * This adds an item to a table with the PrimaryKey as input
     *
     * @param primaryKey
     * @param infoMap
     */
    public void putItemOp(
            PrimaryKey primaryKey,
            Map infoMap) {

        LOG.log(Level.INFO, "Adding a new item...");

        try {
            PutItemSpec spec = new PutItemSpec()
                    .withItem(new Item().withPrimaryKey(primaryKey)
                            .withMap("info", infoMap))
                    // Need to check if the partition key and the range key combination does not exist
                    .withConditionExpression("attribute_not_exists(#yr) AND attribute_not_exists(title)")
                    // "year" is a DynamoDB reserved word. So need to use the express attribute name
                    // syntax to map to the reserved word.
                    .withNameMap(new NameMap().with("#yr", "year"));
            
            table.putItem(spec);
            
        } catch (ConditionalCheckFailedException e) {
            LOG.log(Level.INFO, "Put item failed because conditional check failed");
            
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unable to add item.", e);
        }
    }
    
    public void deleteItem(PrimaryKey primaryKey) {
        try {
            
            table.deleteItem(primaryKey);
            
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Delete item failed", e);
        }
    }
    
    /**
     * 
     * @param scanSpec
     * @return 
     */
    public ItemCollection<ScanOutcome> scanTable(ScanSpec scanSpec) {
        
        ItemCollection<ScanOutcome> scanResult = null;
        
        try {
            
            scanResult = table.scan(scanSpec);
            
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Scan failed", e);
        }
        
        return scanResult;
    }
    
    public List<String> getAllTableAttributes() {
        TableDescription description = table.getDescription();
        List<AttributeDefinition> attributeDefinitions = description.getAttributeDefinitions();
    }
}
